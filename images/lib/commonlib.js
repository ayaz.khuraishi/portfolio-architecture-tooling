/**
 * Reads in the file given and removes excess spaces, tabs,
 * newlines, and returns. This allows templates and other files
 * to be kept in easier for humans to read formats for editing
 * but compresses them for use in the tooling.
 * @param  {string} name     Full path and name to read in
 * @return {string}          The file contents cleaned
 */
function readFileClean (name) {
  let fs = require('fs');
  if (fs.existsSync(name)) {
  	try {
      return fs.readFileSync(name).toString()
    	         .replace(/(\r\n|\n|\r)/gm,"").replace(/\s\s/g,"");
    }
    catch (e) {
      throw new Error('Error reading file: ' + e);
    }
  } else {
    throw new Error('File not found: ' + name);
  }
}

/**
 * Iterates over each library entry creates files as needed. 
 * Creates the mxLibraryEntry for each entry and then returns the final mxLibrary.
 * @param {object} config     Config object from common config
 * @param {object} entry      JSON object entry read from library config
 */
function buildLibrary (element, config) {
  let libraryentries = [];
  console.log("Processing library " + config.configfile.targetlibrary)
  for (i in config.library) {
    element.setup(config.library[i], config);
    element.generateFile();
    libraryentries.push(element.mxLibraryEntry()); 
  }
  // Get the XML for output
  let xmlout = '<mxlibrary>[' + libraryentries.join(',') + ']</mxlibrary>'
  let fs = require('fs')
  try{
    // Check to see if the generated XML has changed. If not then skip this file
    if (readFileClean(config.configfile.targetdir + config.configfile.targetlibrary) == xmlout) {
    console.log("Libray " + config.configfile.targetlibrary + " is unchanged. Skipped.") 
    return 
    }
  }
  catch(e) {
    // If the file is not does not exist previously then it has changed. It's new
    let notFoundMsg = "Error: File not found:"
    if(e.toString().indexOf( notFoundMsg ) >= 0) {
      console.log("** New library file: " + config.configfile.targetlibrary )
    }
    else {
      console.log("*** ERROR. Something else is wrong: " + e)
    }
  }
  // So if it's changed or new then we need to write it out
  fs.writeFile(config.configfile.targetdir + config.configfile.targetlibrary, xmlout, function(err) {
   if (err) {
      return console.error(err)
   }
  })
  // Success message
  console.log(config.configfile.targetlibrary + ". Data written successfully!");
}

module.exports = {
  readFileClean,
  buildLibrary
}
